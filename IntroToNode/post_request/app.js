var express = require("express");
var app = express();

var bodyparser = require("body-parser");

var friends = ["james", "ahmet", "hello", "pierre"];


app.set("view engine", "ejs");

app.use(bodyparser.urlencoded({extended: true}));

app.get('/', function(req, res) {
  res.render("home");
});

app.get('/friends', function(req, res) {
  res.render("friends", {friends: friends});
});

app.post('/addfriend', function(req, res) {
  var newFriend = req.body.newfriend;
  friends.push(newFriend);
  res.redirect("/friends");
});

app.listen(3000, function() {
  console.log("Server starting on port 3000.");
});
