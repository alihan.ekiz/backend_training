var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var methodOverride = require('method-override');

mongoose.connect('mongodb://localhost/myapp');

var app = express();
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: true}));
app.use(methodOverride('_method'));

var personSchema = new mongoose.Schema({
  vorname: String,
  nachname: String,
  stadt: String,
  land: String
});

var Person = mongoose.model('Person', personSchema);

app.get('/', (req, res) => {
  res.render('index');
});

app.get('/people', (req, res) => {
  Person.find({}, (err, data) => {
    if(err) {
      console.log("An error accured :/");
      res.redirect('/');
    } else {
      res.render('people', {people: data});
    }
  })
});

app.get('/people/new', (req, res) => {
  res.render('new');
});

app.post('/people', (req, res) => {
  Person.create(req.body.person, (err, newPerson) => {
    if(err) {
      res.redirect('new');
    } else {
      res.redirect('/people');
    }
  })
});

app.get('/people/:id', (req, res) => {
  Person.findById(req.params.id, (err, foundPerson) => {
    if(err) {
      console.log('An error accured.. :/');
    } else {
      res.render('show', {person: foundPerson});
    }
  });
});

app.get('/people/:id/edit', (req, res) => {
  Person.findById(req.params.id, (err, foundPerson) => {
    if(err) {
      console.log('Error :/');
    } else {
      res.render('edit', { person: foundPerson });
    }
  })
});

app.put('/people/:id', (req, res) => {
  Person.findByIdAndUpdate(req.params.id, req.body.person, (err, newData) => {
    if(err) {
      console.log('Something went wrong bro.');
      res.redirect('/people');
    } else {
      res.redirect('/people');
    }
  });
});

app.listen(3000, () => {
  console.log('Serving server on port 3000. Have fun !');
});
