var express = require('express');
var app = express();
var request = require('request');
var key = "&apikey=thewdb";
var url = "http://omdbapi.com/";

app.set("view engine", "ejs");

app.get('/', function(req, res) {
  res.render("search");
});

app.get('/results', function(req, res) {
  var searchterm = req.query.search;
  var full_url = url+"?s="+searchterm+key;
  request(full_url, function(error, response, body) {
    if(!error && response.statusCode == 200 && body !== undefined) {
      var data = JSON.parse(body);
      res.render("results", {data: data});
    }
  })
});

app.listen(3000, function() {
  console.log("Server started on port 3000. Have fun !");
});
