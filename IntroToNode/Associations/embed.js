var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/post_demo');

var postSchema = new mongoose.Schema({
  title: String,
  content: String
});

var postModel = mongoose.model('Post', postSchema);

var userSchema = new mongoose.Schema({
  email: String,
  name: String,
  posts: [postSchema]
});

var User = mongoose.model('User', userSchema);

User.findOne({name: 'Ahmet'}, (err, user) => {
  if(err) {
    console.log('Error!');
  } else {
    user.posts.push({
      title: '3 things I hate',
      content: 'Voldemort and Voldemort'
    });
    user.save((err, user) => {
      if(err) {
        console.log('An error accured..');
      } else {
        console.log(user);
      }
    });
  }
})

/*var newUser = new User({
  email: 'ahmet@gmx.at',
  name: 'Ahmet',
});

newUser.posts.push({
  title: 'This is a very nice website',
  content: 'I love this website, and I am really happy that I found this.'
});

newUser.save((err, user) => {
  if(err) {
    console.log('Error !');
  } else {
    console.log(user);
  }
});*/

/*var newPost = new postModel({
  title: 'My first post !',
  content: 'This is my very first post here. Cheers !'
});

newPost.save((err, post) => {
  if(err) {
    console.log('Error !');
  } else {
    console.log(post);
  }
});*/
