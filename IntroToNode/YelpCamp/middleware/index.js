var Campground = require('../models/campgrounds');
var Comment = require('../models/comment');

module.exports = {
  checkCampgroundOwnership: function(req, res, next) {
    if (req.isAuthenticated()) {
      Campground.findById(req.params.id, (err, foundCampground) => {
        if(err) {
          res.redirect('back');
        } else {
          if (foundCampground.author.id.equals(req.user._id)) {
            next();
          } else {
            res.redirect('back');
          }
        }
      });
    } else {
      res.redirect('back');
    }
  },

  checkCommentOwnership: function(req, res, next) {
    if (req.isAuthenticated()) {
      Comment.findById(req.params.comment_id, (err, foundComment) => {
        if(err) {
          req.flash('error', 'Campground not found');
          res.redirect('back');
        } else {
          if (foundComment.author.id.equals(req.user._id)) {
            next();
          } else {
            req.flash('error', 'No permission to do that.');
            res.redirect('back');
          }
        }
      });
    } else {
      req.flash('error', 'You have to login first');
      res.redirect('back');
    }
  },

  isLoggedIn: function(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    req.flash('error', 'You have to login first.');
    res.redirect('/login');
  }
};
