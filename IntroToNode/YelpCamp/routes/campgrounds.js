var express = require('express');
var router = express.Router();
var Campground = require('../models/campgrounds');
var middleware = require('../middleware');


router.get('/', (req, res) => {
  Campground.find({}, (err, allCampgrounds) => {
    if (err) {
      console.log("Couldn't load data from database :(");
    } else {
      res.render("campgrounds/index", {
        campgrounds: allCampgrounds
      });
    }
  });
});

router.post('/', middleware.isLoggedIn, (req, res) => {
  var name = req.body.Name;
  var image = req.body.Image;
  var desc = req.body.Description;
  var author = {
    id: req.user._id,
    username: req.user.username
  };
  var price = req.body.Price;
  var newCampground = {
    name: name,
    image: image,
    description: desc,
    author: author,
    price: price
  };
  Campground.create(newCampground, (err, data) => {
    if (err) {
      console.log("Error :(");
    } else {
      console.log(data);
      res.redirect('/campgrounds');
    }
  })
});

router.get('/new', middleware.isLoggedIn, (req, res) => {
  res.render("campgrounds/new");
});

router.get('/:id', (req, res) => {
  Campground.findById(req.params.id).populate('comments').exec((err, data) => {
    if (err) {
      console.log("Eroor :(");
    } else {
      console.log(data);
      res.render("campgrounds/show", {
        campground: data
      });
    }
  });
});

router.get('/:id/edit', middleware.checkCampgroundOwnership, (req, res) => {
  Campground.findById(req.params.id, (err, foundCampground) => {
    res.render('campgrounds/edit', {campground: foundCampground});
  });
});

router.put('/:id', middleware.checkCampgroundOwnership, (req, res) => {
  Campground.findByIdAndUpdate(req.params.id, req.body.campground, (err, updatedCampground) => {
    if(err) {
      res.redirect('/campgrounds');
    } else {
      res.redirect('/campgrounds/' + req.params.id);
    }

  });
});

router.delete('/:id', middleware.checkCampgroundOwnership, (req, res) => {
  Campground.findByIdAndRemove(req.params.id, (err) => {
    if (err) {
      res.redirect('/campgrounds');
    }
    else {
      res.redirect('/campgrounds');
    }
  })
});


module.exports = router;
