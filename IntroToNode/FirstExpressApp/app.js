var express = require('express');
var app = express();

// '/' => "Hi there"
app.get('/', function(req, res) {
  res.send("Hi there!");
});
// 'bye' => "Goodbye"

app.get('/bye', function(req, res) {
  res.send("Goodbye!");
});
// 'dog' => "miau"
app.get('dog', function(req, res) {
  res.send("DOG!!");
});

app.get('/r/:subreditName', function(req, res) {
  var subredit = req.params.subreditName;
  res.send("Welcome to a subredit named" + subredit.toUpperCase());
});

app.get('/r/:subreditName/comments/:id/:title', function(req, res) {
  res.send("Welcome to a comment !");
});

app.get('*', function(req, res) {
  res.send("You are a star !");
});

app.listen(3000, function() {
  console.log("Serving demo on port 3000.");
});
