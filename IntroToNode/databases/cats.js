var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/cat_app');

var catSchema = new mongoose.Schema({
  name: String,
  age: Number
});

var Cat = mongoose.model("Cat", catSchema);
/*
var george = new Cat({
  name: "Hans",
  age: "7"
});

george.save((err, cat) => {
  if(err) {
    console.log("Smth went wrong.");
  } else {
    console.log("You saved a cat to the db ! Great");
    console.log(cat);
  }
});*/

Cat.find({}, (err, cats) => {
  if(err) {
    console.log("There was a error!");
    console.log(err);
  } else {
    console.log("All the cats : ");
    console.log(cats);
  }
});

Cat.create({
  name: "Jessy",
  age: 15
}, (err, cat) => {
  if(err) {
    console.log(err);
  } else {
    console.log("Success !");
  }
});
