var express = require('express');
var app = express();

app.get('/', function(req, res) {
  res.send("Hey there, welcome to my assignment.");
});

app.get('/speak/:animal', function(req, res) {
  var answer;
  var animal = req.params.animal;
  if(animal === "pig") {
    answer = "The pig says oink!";
  } else if(animal === "cow") {
    answer = "The cow says muuh!";
  } else if(animal === "dog") {
    answer = "The dog says wuf wuf!";
  } else {
    answer = "animal not registered.";
  }
  res.send(answer);
});

app.get('/repeat/:word/:times', function(req, res) {
  var num = Number(req.params.times);
  var word = req.params.word;
  var string = "";
  for(var i=0; i<num; ++i) {
    string += word + " ";
  }
  res.send(string);
});

app.get('*', function(req, res) {
  res.send("Bad page. Requested site not found :(");
});

app.listen(3000, function() {
  console.log("Server started at port 3000.");
});
